CREATE TABLE public.student ();

ALTER TABLE public.student ADD id bigint NOT NULL;
ALTER TABLE public.student ADD CONSTRAINT student_pk PRIMARY KEY (id);
ALTER TABLE public.student ADD "name" varchar(30) NULL DEFAULT NULL;
ALTER TABLE public.student ADD email varchar(30) NULL DEFAULT NULL;

create sequence student_seq start 1;

-- INSERT INTO public.student ("name",email) VALUES ('Saul','savazque@redhat.com');