const express = require("express");
const cors = require("cors")
const mysql = require("mysql")
const { Pool } = require('pg');

const app = express();
app.use(express.json());

app.use(cors());

/*
const db = mysql.createConnection({
    host: "localhost",
    user: "user",
    password: "pass",
    database: "db"
})
*/
const db = new Pool({
    user: 'user',
    password: 'pass',
    host: 'localhost',
    port: 5432, // default Postgres port
    database: 'db'
  });

/*
app.get("/", (req, res) => {
    const sql = "SELECT * FROM student";
    db.query(sql, (err, data) => {
        if(err) return res.json("Error");
        return res.json(data);
    })
}
)
*/

app.get("/", (req, res) => {
    const sql = "SELECT * FROM student";
    db.query(sql, (err, data) => {
        if(err) return res.json("Error");
        return res.json(data.rows);
    })
}
)

app.post('/create', (req, res) => {
    const sql = 'INSERT INTO student ("Name", "Email") VALUES ($1, $2)';
    const values = [
        req.body.name,
        req.body.email
    ]
    db.query(sql, values, (err, data) => {
        if(err) return res.json("Error");
        return res.json(data);
    })
})

app.put('/update/:id', (req, res) => {
    const sql = 'UPDATE student SET "Name" = $1, "Email" = $2 WHERE "ID" = $3';
    const values = [
        req.body.name,
        req.body.email
    ]
    const id = req.params.id;
    db.query(sql, [...values, id], (err, data) => {
        if(err) {
            console.log(err)
            return res.json("Error");
        }
        return res.json(data);
    })
})

app.delete('/student/:id', (req, res) => {
    const sql = 'DELETE FROM student WHERE "ID" = $1';
    const id = req.params.id;
    db.query(sql, [id], (err, data) => {
        if(err) return res.json("Error");
        return res.json(data);
    })
})

app.listen(8081, () => {
    console.log("listening");
})

